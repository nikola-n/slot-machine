<?php

namespace App\Console\Commands;

use App\Models\Account;
use Illuminate\Console\Command;

class UpdateSpinsPerDayCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:spins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It updates the spins per day for each user every day.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        Account::query()->chunkById(150, function ($chunk) {
            $chunk->each(function (Account $account) {
                try {
                    $account->update([
                        'spins_per_day' => rand(2, 5),
                    ]);
                    $this->info('Account: ' . $account->username . ' spins were updated!');
                } catch (\Exception $e) {
                    $this->warn($e->getMessage());
                }
            });
        });

        return 0;
    }
}
