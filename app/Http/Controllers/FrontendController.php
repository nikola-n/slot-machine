<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\Validators\LaunchGameValidator;

class FrontendController extends Controller
{
    public function loadCampaign(Campaign $campaign)
    {
        try {
            (new LaunchGameValidator())->validate($campaign, request()->all());
        } catch (\Exception $exception) {
            session(['errors' => $exception->getMessage()]);
            return view('frontend.placeholder');
        }

        return view('frontend.index');
    }

    public function placeholder()
    {
        return view('frontend.placeholder');
    }

}
