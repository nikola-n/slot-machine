<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Campaign extends Model
{

    protected $fillable = [
        'timezone',
        'name',
        'slug',
        'start_date',
        'end_date',
    ];

    protected $dates = [
        'start_date',
        'end_date',
    ];

    protected static function booted()
    {
        static::created(function (Campaign $campaign) {
            $campaign->slug = Str::slug($campaign->name);
            $campaign->save();
        });
    }

    public static function search($query)
    {
        return empty($query) ? static::query()
            : static::where('name', 'like', '%' . $query . '%')
                ->orWhere('timezone', 'like', '%' . $query . '%')
                ->orWhere('starts_at', 'like', '%' . $query . '%')
                ->orWhere('ends_at', 'like', '%' . $query . '%');
    }
}
