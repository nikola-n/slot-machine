<?php

namespace App\Models\Validators;

use App\Models\Symbol;
use Exception;
use App\Models\Campaign;

class LaunchGameValidator
{
    /**
     * @param \App\Models\Campaign $campaign
     * @param array                $attributes
     *
     * @return bool
     * @throws \Exception
     */
    public function validate(Campaign $campaign, array $attributes)
    {
        if ( ! isset($attributes['spins']) && isset($attributes['a'])) {
            throw new Exception('Please select all required parameters.');
        }

        if ($campaign->start_date > now() && ! $campaign->end_date > now()) {
            throw new Exception('The dates are not valid, try again later!');
        }

        if(Symbol::query()->active()->count() < 6) {
            throw new Exception('Add more active symbols!');
        }

        return true;
    }
}
