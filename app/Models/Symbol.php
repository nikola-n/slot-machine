<?php

namespace App\Models;

use Database\Factories\SymbolFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Symbol extends Model
{
    use HasFactory;

    public const ACTIVE = 1;
    public const NOT_ACTIVE = 0;

    protected $fillable = [
        'name',
        'symbol',
        'active',
    ];

    protected $casts = [
        'active' => 'boolean',
    ];

    public function scopeActive(Builder $builder): Builder
    {
        return $builder->where('active', Symbol::ACTIVE);
    }

    public function scopeNotActive(Builder $builder): Builder
    {
        return $builder->where('active', Symbol::NOT_ACTIVE);
    }

    protected static function newFactory(): SymbolFactory
    {
        return SymbolFactory::new();
    }
}
