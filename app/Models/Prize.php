<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Prize extends Model
{
    protected $fillable = [
        'campaign_id',
        'title',
        'description',
        'level',
        'weight',
        'startDate',
        'endDate',
        'points',
        'symbol_id',
        'matches',
    ];

    protected $dates = [
        'startDate',
        'endDate',
    ];

    public static function search($query)
    {
        return empty($query) ? static::query()
            : static::where('name', 'like', '%' . $query . '%');
    }

    public function campaign(): BelongsTo
    {
        return $this->belongsTo(Campaign::class);
    }

    public function symbol(): BelongsTo
    {
        return $this->belongsTo(Symbol::class, 'symbol_id');

    }

}
