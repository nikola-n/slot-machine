<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method generate()
 * @see \App\Utilities\Generator
 */
class ReelSlotGenerator extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'slots';
    }
}
