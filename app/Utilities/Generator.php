<?php

namespace App\Utilities;

class Generator
{
    protected array $symbols;

    public function __construct(array $symbols = [])
    {
        $this->symbols = $symbols;
    }

    /**
     * @return array
     */
    public function generate(): array
    {
        $arr = [];
        for ($i = 0; $i < 3; $i++) {
            shuffle($this->symbols);
            $arr[] = $this->addLine($this->symbols);
        }

        return $arr;
    }

    public function addLine($array): array
    {
        for ($i = 0; $i < 5; $i++) {
            $shortenedArr = array_slice($array, 0, 5);
        }

        return $shortenedArr;
    }
}
