<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('username')->nullable();
            $table->unsignedInteger('spins_per_day')->nullable();
            $table->timestamps();

            $table->index('user_id');
        });

        User::query()->each(function (User $user) {
            $user->account()->create([
                'username'      => $user->name,
                'spins_per_day' => config('slot.default_spins_per_day'),
            ]);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
