<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPrizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prizes', function (Blueprint $table) {
            $table->foreignId('symbol_id')->after('campaign_id')->nullable();
            $table->unsignedInteger('matches')->after('level')->nullable();
            $table->unsignedInteger('points')->after('weight')->nullable();

            $table->index('symbol_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prizes', function (Blueprint $table) {
            $table->dropColumn('symbol_id');
            $table->dropColumn('matches');
            $table->dropColumn('points');
        });
    }
}
