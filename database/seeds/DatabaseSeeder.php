<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Prize;
use App\Models\Symbol;
use App\Models\Campaign;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'name'     => 'Test',
            'email'    => 'test@thunderbite.com',
            'level'    => 'full',
            'password' => Hash::make('test123'),
        ]);

        $start = now()->startOfDay();
        $end   = now()->addDays(7)->endOfDay();

        Campaign::query()
            ->create([
                'timezone'   => 'Europe/London',
                'name'       => 'Darts',
                'start_date' => $start,
                'end_date'   => $end,
            ]);

        $campaign = Campaign::query()
            ->create([
                'timezone'   => 'Europe/London',
                'name'       => '5 Reels Slots',
                'start_date' => $start,
                'end_date'   => $end,
            ]);

        Prize::query()
            ->insert([
                [
                    'campaign_id' => $campaign->id,
                    'title'       => 'Low 1',
                    'level'       => 'low',
                    'weight'      => '25.00',
                    'startDate'   => $start,
                    'endDate'     => $end,
                ],
                [
                    'campaign_id' => $campaign->id,
                    'title'       => 'Low 2',
                    'level'       => 'low',
                    'weight'      => '25.00',
                    'startDate'   => $start,
                    'endDate'     => $end,
                ],
                [
                    'campaign_id' => $campaign->id,
                    'title'       => 'Low 3',
                    'level'       => 'low',
                    'weight'      => '50.00',
                    'startDate'   => $start,
                    'endDate'     => $end,
                ],
                [
                    'campaign_id' => $campaign->id,
                    'title'       => 'Med 1',
                    'level'       => 'med',
                    'weight'      => '25.00',
                    'startDate'   => $start,
                    'endDate'     => $end,
                ],
                [
                    'campaign_id' => $campaign->id,
                    'title'       => 'Med 2',
                    'level'       => 'med',
                    'weight'      => '25.00',
                    'startDate'   => $start,
                    'endDate'     => $end,
                ],
                [
                    'campaign_id' => $campaign->id,
                    'title'       => 'Med 3',
                    'level'       => 'med',
                    'weight'      => '50.00',
                    'startDate'   => $start,
                    'endDate'     => $end,
                ],
                [
                    'campaign_id' => $campaign->id,
                    'title'       => 'High 1',
                    'level'       => 'high',
                    'weight'      => '25.00',
                    'startDate'   => $start,
                    'endDate'     => $end,
                ],
                [
                    'campaign_id' => $campaign->id,
                    'title'       => 'High 2',
                    'level'       => 'high',
                    'weight'      => '25.00',
                    'startDate'   => $start,
                    'endDate'     => $end,
                ],
                [
                    'campaign_id' => $campaign->id,
                    'title'       => 'High 3',
                    'level'       => 'high',
                    'weight'      => '50.00',
                    'startDate'   => $start,
                    'endDate'     => $end,
                ],
            ]);

        $this->call(GamesTableSeeder::class);
        Symbol::factory(11)->create();

        User::query()->each(function (User $user) {
            $user->account()->create([
                'username'      => $user->name,
                'spins_per_day' => config('slot.default_spins_per_day'),
            ]);
        });
    }
}
